package com.example.hw_10_activity_and_layouts

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView


val images: Map<String, Int> = mapOf(
    "unown" to R.drawable.unown,
    "squirtle" to R.drawable.squirtle,
    "wartortle" to R.drawable.wartortle,
    "blastoise" to R.drawable.blastoise,
    "mega_blastoise" to R.drawable.mega_blastoise,
    "gigantamax_blastoise" to R.drawable.gigantamax_blastoise
)

class InfoActivity : AppCompatActivity() {

    private lateinit var ivAgeBasedImage: ImageView
    private lateinit var tvUserAge: TextView
    private lateinit var tvPokemonName: TextView

    private lateinit var tvFullName: TextView
    private lateinit var tvHobbyDescription: TextView

    private lateinit var tvPokemonInformation: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        val lastName = intent.getStringExtra("last_name")
        val firstName = intent.getStringExtra("first_name")
        val secondName = intent.getStringExtra("second_name")
        val ageTemp = intent.getStringExtra("age")
        val age = if (ageTemp != "") ageTemp!!.toInt() else -1
        val hobbyDescription = intent.getStringExtra("hobby")

        tvUserAge = findViewById(R.id.tvUserAge)
        tvPokemonName = findViewById(R.id.tvPokemonName)
        ivAgeBasedImage = findViewById(R.id.ivAgeBasedImage)

        tvFullName = findViewById(R.id.tvFullName)
        tvPokemonInformation = findViewById(R.id.tvPokemonInformation)
        tvHobbyDescription = findViewById(R.id.tvHobbyDescription)

        tvPokemonInformation = findViewById(R.id.tvPokemonInformation)



        fillHeader(age)
        fillFullName(lastName, firstName, secondName)
        tvHobbyDescription.text = if (hobbyDescription != "") hobbyDescription else "No hobby specified"
        fillPokemonInformation(age)
    }

    private fun fillPokemonInformation(age: Int) {
        tvPokemonInformation.text = getString( when ( getPokemonByAge(age) ) {
            "squirtle" -> R.string.squirtle_description
            "wartortle" -> R.string.wartortle_description
            "blastoise" -> R.string.blastoise_description
            "mega_blastoise" -> R.string.mega_blastoise_description
            "gigantamax_blastoise" -> R.string.gigantamax_blastoise_description
            else -> R.string.unown_description
        })
    }

    private fun fillHeader(age: Int) {
        val pokemonName = getPokemonByAge(age)

        ivAgeBasedImage.setImageDrawable(getDrawable(images[pokemonName]!!))
        tvUserAge.text = if(pokemonName != "unown") age.toString() else "<*#&?&#*>"
        tvPokemonName.text = pokemonName.replace('_', ' ')
    }

    private fun fillFullName(lastName: String?, firstName: String?, secondName: String?) {
        val fullname = "${lastName ?: ""} ${firstName ?: ""} ${secondName ?: ""}"
        tvFullName.text = if(fullname != "  ") fullname else "No name specified"
    }

    private fun getPokemonByAge(age: Int): String = when (age) {
        in 1..10 -> "squirtle"
        in 11..18 -> "wartortle"
        in 19..40 -> "blastoise"
        in 40..65 -> "mega_blastoise"
        in 66..100 -> "gigantamax_blastoise"
        else -> "unown"
    }
}