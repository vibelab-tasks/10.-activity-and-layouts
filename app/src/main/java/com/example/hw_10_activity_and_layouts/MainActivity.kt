package com.example.hw_10_activity_and_layouts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var tvLastNameLabel: TextView
    private lateinit var tvFirstNameLabel: TextView
    private lateinit var tvSecondNameLabel: TextView
    private lateinit var tvAgeLabel: TextView
    private lateinit var tvHobbyLabel: TextView

    private lateinit var etLastName: EditText
    private lateinit var etFirstName: EditText
    private lateinit var etSecondName: EditText
    private lateinit var etAge: EditText
    private lateinit var etHobby: EditText

    private lateinit var btnInfoActivity: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etLastName = findViewById(R.id.etLastName)
        etFirstName = findViewById(R.id.etFirstName)
        etSecondName = findViewById(R.id.etSecondName)
        etAge = findViewById(R.id.etAge)
        etHobby = findViewById(R.id.etHobby)
        btnInfoActivity = findViewById(R.id.btnInfoActivity)

        btnInfoActivity.setOnClickListener {
            val intent = Intent(this, InfoActivity::class.java)

            intent.putExtra("first_name", etLastName.text.toString())
            intent.putExtra("last_name", etFirstName.text.toString())
            intent.putExtra("second_name", etSecondName.text.toString())
            intent.putExtra("age", etAge.text.toString())
            intent.putExtra("hobby", etHobby.text.toString())

            startActivity(intent)
        }

    }
}